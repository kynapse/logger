/*
 * Kenneth Lam      500462876
 * Christopher Chan 500446553
 * Peter Le Quang   500443245
 */
package logger.plan;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

/**
 *
 * @author Kenneth
 */
public class Plan {

    static int[][][][] transitions;
    static int[][] maxProfit;
    static directionEnum[][] travelDirection;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("input.txt");
        int size = 0;

        //debug code
//        Path currentRelativePath = Paths.get("");
//        String s = currentRelativePath.toAbsolutePath().toString();
//        System.out.println("Current relative path is: " + s);

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            //read length
            size = Integer.parseInt(reader.readLine());
            transitions = new int[size][size][size][size];

            String line;
            //read rest of file
            while ((line = reader.readLine()) != null) {
                Pattern linePattern = Pattern.compile(" ");
                String input[] = linePattern.split(line, 5);
                int integerinput[] = new int[5];
                for (int i = 0; i < 5; i++) {
                    integerinput[i] = Integer.parseInt(input[i]);
                }
                transitions[integerinput[0]][integerinput[1]][integerinput[2]][integerinput[3]] = integerinput[4];
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        //process array
        maxProfit = new int[size][size]; //maximum profit amount for each tile
        travelDirection = new directionEnum[size][size]; //direction to travel in
//        for (Iterator it = Arrays.asList(maxProfit).iterator();it.hasNext();){
//            int[] element = (int[]) it.next();
//            for (Iterator it2 = Arrays.asList(it).iterator(); it2.hasNext();) {
//                int element2 = (int) it.next();
//                it2.
//                element2 = Integer.MIN_VALUE;
//            }
//        }
        //initialize array to MIN_VALUE
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                maxProfit[i][j] = Integer.MIN_VALUE;
                travelDirection[i][j] = directionEnum.NONE;
            }
        }


        for (int i = size - 1; i >= 0; i--) {
            for (int j = 0; j < size; j++) {
                getMaxProfit(j, i, size);
            }
        }
        for (int i = size - 1; i >= 0; i--) { //start from top
            for (int j = 0; j < size; j++) { //start from left
                System.out.printf("%+11d %10s", maxProfit[j][i], travelDirection[j][i].toString());
            }
            System.out.println();
        }
        System.out.println();

        //print optimal route
        int greatestIndex = Integer.MIN_VALUE;
        int greatestValue = Integer.MIN_VALUE;
        for (int i = 0; i < size; i++) {
            if (greatestValue < maxProfit[i][0]) {
                greatestIndex = i;
                greatestValue = maxProfit[i][0];
            }
        }
        int i = greatestIndex; //horizontal position
        int j = 0; //vertical position
        System.out.printf("Start at position %d\n", i);
        while (travelDirection[i][j] != directionEnum.NONE) {
            System.out.printf("Move %s\n", travelDirection[i][j].toString());
            if (travelDirection[i][j] == directionEnum.NORTHEAST) {
                i++;
            } else if (travelDirection[i][j] == directionEnum.NORTHWEST) {
                i--;
            }
            j++;
        }
        System.out.println("Finish.");
    }

    private static void getMaxProfit(int i, int j, int size) {
        if ((j == size - 1)) {
            maxProfit[i][j] = 0;
        } else {
            int north = maxProfit[i][j + 1] + transitions[i][j][i][j + 1];
            int west = Integer.MIN_VALUE;
            int east = Integer.MIN_VALUE;
            if (i > 0) {
                west = maxProfit[i - 1][j + 1] + transitions[i][j][i - 1][j + 1];
            }
            if (i < size - 1) {
                east = maxProfit[i + 1][j + 1] + transitions[i][j][i + 1][j + 1];
            }

            if (north > west) {
                if (north > east) {
                    //north greatest
                    maxProfit[i][j] = north;
                    travelDirection[i][j] = directionEnum.NORTH;
                } else {
                    //east greatest
                    maxProfit[i][j] = east;
                    travelDirection[i][j] = directionEnum.NORTHEAST;
                }
            } else if (west > east) {
                //west greatest
                maxProfit[i][j] = west;
                travelDirection[i][j] = directionEnum.NORTHWEST;
            } else {
                //east greatest
                maxProfit[i][j] = east;
                travelDirection[i][j] = directionEnum.NORTHEAST;
            }
        }
    }
}
