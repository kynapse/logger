/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package logger.plan;

/**
 * Directions for the to move.
 * @author Kenneth
 */
public enum directionEnum {
    /**
     * Indicates movement in a northerly direction.
     */
    NORTH,
    /**
     * Indicates movement in a northeasterly direction.
     */
    NORTHEAST,
    /**
     * Indicates movement in a northwesterly direction.
     */
    NORTHWEST,
    /**
     * Indicates NoOp or no movement from location
     */
    NONE

}
